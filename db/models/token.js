const mongoose = require('mongoose');

const tokenSchema = new mongoose.Schema({
	accessToken: String,
	accessTokenExpiresAt: Date,
	client: Object,
	user: Object
})

const Token = mongoose.model('Token',tokenSchema);

module.exports = Token;