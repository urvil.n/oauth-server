const mongoose = require('mongoose');

const clientSchema = new mongoose.Schema({
    id: String,
    clientId: {
        type: String,
        required: true
    },
    clientSecret: {
        type: String,
        required: true
    },
    grants: {
        type: [String],
        default: ['password']
    },
    redirectUris: [String]
})

const Client = mongoose.model('Client', clientSchema);

module.exports = Client;