const mongoose = require('mongoose');

const authorizaSchema = new mongoose.Schema({
	authorizationCode: String,
	authorizationCodeExpriesAt: Date,
	redirectUri: String,
	client: Object,
	user: Object
})

const Authorization = mongoose.model('Authorization', authorizaSchema);

module.exports = Authorization;