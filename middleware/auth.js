const OAuth2Server = require('oauth2-server');
const Request = OAuth2Server.Request;
const Response = OAuth2Server.Response;

const oauth = new OAuth2Server({
    model: require('../model'),
    accessTokenLifetime: 60 * 60,
    allowBearerTokensInQueryString: true
});

const auth = async (req, res, next) => {
    try {
        const request = new Request(req);
        const response = new Response(res);

        const token = await oauth.authenticate(request, response);
        if (!token) throw new Error();
        next();
    } catch (err) {
        res.send({ error: 'Please Authenticate' });
    }
}

module.exports = auth;