const clientModel = require('./db/models/client');
const tokenModel = require('./db/models/token');
const userModel = require('./db/models/user');
const authorizaionModel = require('./db/models/authorizationCode');

const getAccessToken = async (token) => {
	const Token = await tokenModel.findOne({
		accessToken: token
	});
	console.log('getAccessToken', Token);
	return Token;
};

const getAuthorizationCode = async (authorizationCode) => {
	const code = await authorizaionModel.findOne({
		authorizationCode
	});
	console.log('getAuthorizationCode', code);
	return code;
};

const getClient = async (clientId, clientSecret) => {
	const client = await clientModel.findOne({
		clientId,
		clientSecret
	});
	console.log('getClient', client);
	return client;
};

const saveToken = async (token, client, user) => {
	token.client = { id: client.clientId };
	token.user = { username: user.username };

	const Token = new tokenModel(token);
	const result = await Token.save();
	console.log('saveToken', result);
	return result;
};

const saveAuthorizationCode = async (code, client, user) => {
	code.client = { id: client.clientId };
	code.user = { username: user.username };

	const Code = new authorizaionModel(code);
	const result = await Code.save();
	console.log('saveCode', result);
	return result;
};

const revokeAuthorizationCode = async (code) => {
	await authorizaionModel.deleteOne({ authorizationCode: code });
	console.log('revoke');
	return true;
}

const getUser = async (username, password) => {
	const user = await userModel.findOne({
		username,
		password
	});
	console.log('getUser', user);
	return user;
};

module.exports = {
	getAccessToken,
	getClient,
	saveToken,
	getUser,
	getAuthorizationCode,
	saveAuthorizationCode,
	revokeAuthorizationCode
};