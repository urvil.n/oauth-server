const express = require('express');
require('./db/mongoose-connection')
const OAuth2Server = require('oauth2-server');
const bodyParser = require('body-parser');
const auth = require('./middleware/auth');
const Client = require('./db/models/client');
const User = require('./db/models/user');
const Request = OAuth2Server.Request;
const Response = OAuth2Server.Response;

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.oauth = new OAuth2Server({
	model: require('./model'),
	accessTokenLifetime: 60 * 60,
	allowBearerTokensInQueryString: true
});

app.all('/oauth/token', async (req, res) => {
	try {
		const request = new Request(req);
		const response = new Response(res);

		const token = await app.oauth.token(request, response);
		res.send(token);
	} catch (err) {
		res.send('something went wrong');
	}
});

app.get('/', auth, (req, res) => {
	res.send('Congratulations, you are in a secret area!');
});

app.post('/create/client', async (req, res) => {
	try {
		const client = new Client(req.body);
		const result = await client.save();
		res.send(result);
	} catch (err) {
		res.send(err.message);
	}
})

app.post('/create/user', async (req, res) => {
	try {
		const user = new User(req.body);
		const result = await user.save();
		res.send(result);
	} catch (err) {
		res.send(err.message);
	}
})

// app.post('/oauth/authorize', async (req, res) => {
// 	try{
// 		const request = new Request(req);
// 		const response = new Response(res);
// 		console.log(request.query);
// 		const code = await app.oauth.authorize(request, response);
// 		res.send(code);
// 	} catch(err) {
// 		console.log(err);
// 		res.send('something went wrong');
// 	}
// });

const port = process.env.PORT || 3000;
app.listen(port, () => {
	console.log(`server listening on ${port}`);
})